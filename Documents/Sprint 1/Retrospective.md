#Sprint 1 Retrospective

##Safety Check

We conducted a safety check as specified in the scrum training videos. On a scale of 0-4 team members 
provided their feedback varying from “Most of my best ideas are not discussable” to “Everything is 
discussable without filtering.” Most team members felt that they were able to express themselves in a 
free manner. No team members felt that they had no opportunities to provide their ideas for group 
consideration.

##What did you learn?

Generally speaking the team felt that they were more comfortable with the Scrim process, time management, 
and how to work with a team.  The code organization is coming together, and everyone is getting a better 
understanding about how to work with each other’s code. We’re learning how to divide up story effort 
points so that team members all have enough to work on.

##What still puzzles you?
Team members were not entirely sure how to assigned specific utterances to LUIS. There was some concern 
from team members that the number of meetings required to keep everyone organized was excessive and 
difficult to maintain. Team members were not entirely sure how the code triggers the microphone inside 
the webaudio API. 

##How can we improve?
During this sprint there was some task overlap that could have been avoided by team members managing the 
scope of their work when processing tasks. Overall most team members felt we were doing well working as a 
group. One team member was concerned about the lack of focus during meeting. Team members felt that the 
communication during the actual sprint process could be improved.

##Are there any items that need to be brought up with someone outside the team?
At least one team member felt that Scot needed to know how stressful and potentially unreasonable the 
workload for this project-based class is.
