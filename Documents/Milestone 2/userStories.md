## User Stories ##
1. As an admin, I want to be able to add and remove athletes so that we are tracking only current athletes.
2. As an admin, I want to be able to access login data so that I may reset lost passwords for athletes, coaches, and parents.
3. As an athlete, I want to view my distance ran so that I can see my improvement.
4. As an athlete, I want to track my speed so that I can see my improvement.
5. As an athlete, I want to easily upload my info so I can spend more time training.
6. As an athlete, I want to I want to log in to see my data so I can see where I need to improve.
7. As a coach, I want to track equipment so that I can know what athlete on my team has which set of gear.
8. As a coach, I want to be able to log into the system so that I can view athlete data for my team.
9. As a coach, I want to be able to view all athlete data for my team so that I can improve athlete training.
10. As a parent, I want to be able to verify that my child is going to training sessions so that I can know they are living up to their commitments.
11. As a parent, I want to view my child's athletic information so that I can make sure my child is healthy.
12. As a parent, I want my child's information is protected from other students and parents so that I can protect them from bullying.
