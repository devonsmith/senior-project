## Requirements ##
#### How do we want to handle authentication? ####
School-based SSO system (Active directory) using the athlete’s assigned student identification number?
#### What should the run routes look like? ####
Overlay the run route on top of a google map.
#### What information should be available to the administrators? ####
They can see if the student has an upload for a specific day, and be able to view the DBMS changelog. Which student has which equipment checked out, but they should not be able to see the actual run data for the students.
#### What information should be available to the parents? ####
The parents should be able to see the heart rate, distance, calories burned, and where they ran (allows them to make sure they’re not running next to dangerous highways). They should also be able to see who has access to their student’s information.
#### How are parents going to know if their child went to practice? ####
The information that the student uploads from their smart watch will have a timestamp on it and parents will be able to see if the student has a run for any specific day.
#### What types of information do we get from the Garmin fitness trackers? ####
Since we don’t currently have a fitness tracker file, let’s assume we get times, distance travelled, calories burned, heart rate, and some unstructured GPS data.
#### How will we collect the information from the athletes? ####
The athlete at the end of a training session will get on a web portal and upload the file from their Garmin fitness tracker.
### List of Non-Functional Requirements ###
#### Storage Requirements  ####
Temporary file storage for processing the information file (possibly in the database as unstructured text data). 
#### User Access Controls and Privacy Rules ####
User login using a web form and SSO.
#### Database System ####
MS SQL hosted in Microsoft Azure. Using a new database that may connect to other database systems such as school attendance systems.
#### Other Requirements ####
Hardware for biometric telemetry and GPS data.

