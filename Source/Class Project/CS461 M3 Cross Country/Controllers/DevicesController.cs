﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CS461_M3_Cross_Country.DAL;

namespace CS461_M3_Cross_Country.Controllers
{
    /// <summary>
    /// Controller for Device interaction. Allows users of various types to add or remove the 
    /// devices from the database and checkout devices to athletes on the team. 
    /// </summary>
    public class DevicesController : Controller
    {

        private XCDatabaseContext db = new XCDatabaseContext();
        
        #region Administrator Views

        /// <summary>
        /// Provides a list of all the devices that are in the system. Intended for use by 
        /// administrators.
        /// TODO: Add authentication
        /// </summary>
        /// <returns>a view with a list of all the devices in the system.</returns>
        public ActionResult Index()
        {
            var devices = db.Devices.Include(d => d.Athlete);
            return View(devices.ToList());
        }

        /// <summary>
        /// Provides detailed information about a device. This is currently just a overview of the device.
        /// Could be used by students to check the status of the devices checked out to them.
        /// </summary>
        /// <param name="id">The identification number of the device in the database.</param>
        /// <returns>A view of a specific device.</returns>
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            return View(device);
        }

        /// <summary>
        /// Allows administrators to add a device to the system.
        /// </summary>
        /// <returns>A view containing a new empty device.</returns>
        public ActionResult Create()
        {
            Device device = new Device();
            ViewBag.AssignedTo = new SelectList(db.Athletes, "AthleteID", "LastName", "FirstName", device.AssignedTo);
            return View(device);
        }

       /// <summary>
       /// Creates the new device for addition to the database.
       /// </summary>
       /// <param name="device">The device object created to be added to the database.</param>
       /// <returns>A view with the device that was created.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DeviceID,Description,SerialNumber,Manufacturer,PurchaseDate,Damaged,AssignedTo")] Device device)
        {
            if (ModelState.IsValid)
            {
                db.Devices.Add(device);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AssignedTo = new SelectList(db.Athletes, "AthleteID", "LastName", "FirstName", device.AssignedTo);
            return View(device);
        }

        /// <summary>
        /// Editor for device properties. This is intended for use by an administrator.
        /// </summary>
        /// <param name="id">The identification number of the devices being edited.</param>
        /// <returns>A view containing the device to be edited.</returns>
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            ViewBag.AssignedTo = new SelectList(db.Athletes, "AthleteID", "LastName", "FirstName", device.AssignedTo);
            return View(device);
        }

        /// <summary>
        /// Changes the properties of the device in the database.
        /// </summary>
        /// <param name="device">the device object that will be edited in the database.</param>
        /// <returns>A view of the device that was edited.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DeviceID,Description,SerialNumber,Manufacturer,PurchaseDate,Damaged,AssignedTo")] Device device)
        {
            if (ModelState.IsValid)
            {
                db.Entry(device).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AssignedTo = new SelectList(db.Athletes, "AthleteID", "FirstName", device.AssignedTo);
            return View(device);
        }

        /// <summary>
        /// Enabled the removal of devices from the database. This is intended for administrator use and allows
        /// them to remove devices from the system if lost/damaged/replaced.
        /// </summary>
        /// <param name="id">The identifier of the device in the database.</param>
        /// <returns>A view containing the device to be deleted.</returns>
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            return View(device);
        }

        /// <summary>
        /// Deletes the specified device from the database.
        /// </summary>
        /// <param name="id"> The identification number of the device to be removed from the database.</param>
        /// <returns>RedirectToAction that returns to Devices/Index.</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Device device = db.Devices.Find(id);
            db.Devices.Remove(device);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion

        #region Coach-specific Views


        /// <summary>
        /// Device list for the coaches. 
        /// </summary>
        /// <returns>A view containing a list of all the devices in the system.</returns>
        public ActionResult CoachDevices()
        {
            var devices = db.Devices.ToList();
            return View(devices);
        }

        /// <summary>
        /// Editor for devices intended for use by coaches.
        /// </summary>
        /// <param name="id">The identifier of the device.</param>
        /// <returns>A view of he device being edited.</returns>
        public ActionResult CoachDevicesEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            ViewBag.AssignedTo = new SelectList(db.Athletes, "AthleteID", "LastName", "FirstName", device.AssignedTo);
            return View(device);
        }

        /// <summary>
        /// Edits the device in the database. Intended for use by the coaches.
        /// </summary>
        /// <param name="device">The device object that is being edited.</param>
        /// <returns>A view of the device that was edited.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CoachDevicesEdit([Bind(Include = "DeviceID,Description,SerialNumber,Manufacturer,PurchaseDate,Damaged,AssignedTo")] Device device)
        {
            if (ModelState.IsValid)
            {
                db.Entry(device).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("CoachDevices");
            }
            return View(device);
        }
        #endregion


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
