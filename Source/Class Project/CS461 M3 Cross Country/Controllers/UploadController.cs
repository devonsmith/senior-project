﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CS461_M3_Cross_Country.Controllers
{
    public class UploadController : Controller
    {
        // GET: /
        // GET: /Index
        // GET: Upload
        public ActionResult Index(int? id)
        {
            if (id == 1)
            {
                ViewBag.Message = "File uploaded Successfully!";
            }
            return View();
        }

        // POST: Upload
        [HttpPost]
        public ActionResult Index(HttpPostedFileBase file)
        {
            try
            {
                if (file.ContentLength > 0)
                {
                    string _FileName = Path.GetFileName(file.FileName);
                    string _path = Path.Combine(Server.MapPath("~/UploadedFiles"), _FileName);
                    // Don't allow it to save the file, toss it for now since we don't have storage
                    //file.SaveAs(_path);
                }
                //ViewBag.Message = "File uploaded Successfully!";
                //return View();
                return RedirectToAction("Uploading");
            }
            catch
            {
                ViewBag.Message = "File upload FAILED!";
                return View();
            }
        }

        // GET: Uploading
        [HttpGet]
        public ActionResult Uploading()
        {
            return View();
        }
    }
}