namespace CS461_M3_Cross_Country.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Device
    {
        public int DeviceID { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        [Required]
        [StringLength(20)]
        [Display(Name = "Serial Number")]
        public string SerialNumber { get; set; }

        [Required]
        [StringLength(35)]
        public string Manufacturer { get; set; }

        [Required]
        [Column(TypeName = "datetime")]
        [Display(Name = "Purchase Date")]
        public DateTime PurchaseDate { get; set; }

        [Display(Name = "Device Damaged")]
        public bool Damaged { get; set; }

        [Display(Name = "Assigned To")]
        public int? AssignedTo { get; set; }

        public virtual Athlete Athlete { get; set; }
    }
}
