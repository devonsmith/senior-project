﻿using NUnit.Framework;
using Moq;
using Remi.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Remi.Models.DAL;
using Remi.Models;

namespace Remi.Controllers.Tests
{
    [TestFixture()]
    public class UserControllerTests
    {
        private Mock<IRepository> mock;
        
        [SetUp]
        public void SetupUserMock()
        {
            mock = new Mock<IRepository>();
            mock.Setup(u => u.GetUserByUserID(1)).Returns(new User() { UserID = 1, UserName = "TestUser", Email = "test@test.com", CipheredPassword = "Xyum7g+Tt7u+ZigACded/Hdl8UzY7l5CwROQ7968jfJjQWSiWSfIn/TNRmkKkiLM7Xo8G+1S20WuBOFPfBwWXanuuYekV8AlWvuKG+xI4xIu8Vwkh4UjDk/KPU9VVLTd", PasswordSalt = "PasswordSalt" });
            mock.Setup(u => u.GetUserByEmail("test@test.com")).Returns(new User() { UserID = 2, UserName = "TestUser", Email = "test@test.com", CipheredPassword = "Xyum7g+Tt7u+ZigACded/Hdl8UzY7l5CwROQ7968jfJjQWSiWSfIn/TNRmkKkiLM7Xo8G+1S20WuBOFPfBwWXanuuYekV8AlWvuKG+xI4xIu8Vwkh4UjDk/KPU9VVLTd", PasswordSalt = "PasswordSalt" });
            mock.Setup(p => p.GetProfileByID(1)).Returns(new Profile() { ProfileID = 1, Alias = "Test", VoicePref = "UK English Male", CalendarID = "reviaitest@gmail.com", HomeLoc = "Monmouth, Oregon", HomeCity = "Monmouth", HomeState = "Oregon", UserID = 1 });
            mock.Setup(p => p.GetProfileByUserID(1)).Returns(new Profile() { ProfileID = 1, Alias = "Test", VoicePref = "UK English Male", CalendarID = "reviaitest@gmail.com", HomeLoc = "Monmouth, Oregon", HomeCity = "Monmouth", HomeState = "Oregon", UserID = 1 });
        }

        // Jake's Test
        // Feature 126
        [Test()]
        public void TestGetUserByUserIdWhereIdCorrectReturnsUser()
        {
            //Arrange
            UserController controller = new UserController(mock.Object);
            int id = 1;
            //Act
            User u = mock.Object.GetUserByUserID(id);
            //Assert
            Assert.AreEqual(u.Email, "test@test.com"); // That(u.Email, Equals("test@test.com"));
        }

        // Jake's Test
        // Feature 126
        [Test()]
        public void TestGetUserByUserIdWhereIdIncorrectReturnsNull()
        {
            //Arrange
            UserController controller = new UserController(mock.Object);
            int id = 2;
            //Act
            User u = mock.Object.GetUserByUserID(id);
            //Assert
            Assert.IsNull(u); // AreEqual(u.Email, "test@test.com"); // That(u.Email, Equals("test@test.com"));
        }

        // Jake's Test
        // Feature 126
        [Test()]
        public void TestGetUserByEmailWhereEmailCorrectReturnsUser()
        {
            //Arrange
            UserController controller = new UserController(mock.Object);
            string email = "test@test.com";
            //Act
            var u = mock.Object.GetUserByEmail(email);
            //Assert
            Assert.AreEqual(u.Email, "test@test.com"); // That(u.Email, Equals("test@test.com"));
        }

        // Jake's Test
        // Feature 126
        [Test()]
        public void TestGetUserByEmailWhereEmailIncorrectReturnsNull()
        {
            //Arrange
            UserController controller = new UserController(mock.Object);
            string email = "InvalidEmail@test.com";
            //Act
            User u = mock.Object.GetUserByEmail(email);
            //Assert
            Assert.IsNull(u); // AreEqual(u.Email, "test@test.com"); // That(u.Email, Equals("test@test.com"));
        }

        // Jake's Test
        // Feature 126
        [Test()]
        public void TestGetUserByEmailWhereEmailCorrectAssertUserNameEmailAndPasswordNotNull()
        {
            //Arrange
            UserController controller = new UserController(mock.Object);
            string email = "test@test.com";
            //Act
            var u = mock.Object.GetUserByEmail(email);
            //Assert
            Assert.IsNotNull(u.UserName);
            Assert.IsNotNull(u.Email);
            Assert.IsNotNull(u.CipheredPassword);
        }

        // Jake's Test
        // Feature 258
        [Test()]
        public void TestProfileVoicePrefReturnsCorrectVoicePref()
        {
            //Arrange
            UserController controller = new UserController(mock.Object);
            int id = 1;
            //Act
            Profile p = mock.Object.GetProfileByUserID(id);
            //Assert
            Assert.AreEqual(p.VoicePref, "UK English Male");
        }

        // Jake's Test
        // Feature 258
        [Test()]
        public void TestProfileHomeCityReturnsCorrectHomeCity()
        {
            //Arrange
            UserController controller = new UserController(mock.Object);
            int id = 1;
            //Act
            Profile p = mock.Object.GetProfileByUserID(id);
            //Assert
            Assert.AreEqual(p.HomeCity, "Monmouth");
        }

        // Jake's Test
        // Feature 258
        [Test()]
        public void TestProfileHomeStateReturnsCorrectHomeState()
        {
            //Arrange
            UserController controller = new UserController(mock.Object);
            int id = 1;
            //Act
            Profile p = mock.Object.GetProfileByUserID(id);
            //Assert
            Assert.AreEqual(p.HomeState, "Oregon");
        }
    }
}