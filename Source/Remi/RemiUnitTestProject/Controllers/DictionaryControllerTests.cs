﻿using NUnit.Framework;
using Remi.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Remi.Controllers.Tests
{
    [TestFixture()]
    public class DictionaryControllerTests
    {
        private DictionaryController c;
        private string shortInput;
        private string longInput;
        private string numberInput;
        private string capInput;
        private string expectedResult;
        private string expectedNum;
        private string nullInput;

        [SetUp()]
        protected void SetUp()
        {
            c = new DictionaryController();
            shortInput = "definition";
            longInput = "definition again";
            numberInput = "123definition";
            capInput = "DEFinITioN";
            expectedResult = "definition";
            expectedNum = "123definition";
            nullInput = null;
        }
        
        // Abby test 1
        [Test()]
        public void GetWordIDTest_ReturnsSameWord()
        {
            string actualResult = c.GetWordID(shortInput);
            Assert.AreEqual(actualResult, expectedResult);
        }

        // Abby test 2
        [Test()]
        public void GetWordIDTest_ReturnsOnlyFirstWord()
        {
            string actualResult = c.GetWordID(longInput);
            Assert.AreEqual(actualResult, expectedResult);
        }

        // Abby test 3
        [Test()]
        public void GetWordIDTest_NumbersInWordPasses()
        {
            string actualResult = c.GetWordID(numberInput);
            Assert.AreEqual(actualResult, numberInput);
        }

        // Abby test 4
        [Test()] 
        public void GetWordIDTest_ReturnsLowercase()
        {
            string actualResult = c.GetWordID(capInput);
            Assert.AreEqual(actualResult, expectedResult);
        }

        // Abby test 5
        [Test()]
        public void GetWordID_NullInputReturnsNull()
        {
            string actualResult = c.GetWordID(nullInput);
            Assert.AreEqual(actualResult, null);
        }
    }
}