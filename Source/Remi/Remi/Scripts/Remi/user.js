﻿//Global Variables Here
var currUser;
var home;
var calendarID;
var voicePref;
var uid;

//Listeners Here

//clicked the login/user link
$('#userLogin').click(userLoginPrompt);

//clicked the logout link
$('#userLogout').click(showLogout);

// Logout confirmation button
$('#logoutConfirm').click(userLogout);
$('#logoutCancel').click($('#logoutConfirmation').modal('hide'));

// Login submit button
$('#loginSubmit').click(userLogin);

// Create account link
$('#createLink').click(createAccount);

// Create account submit button
$('#createSubmit').click(validateAccount);

// Edit user profile button
$('#editProfileButton').click(openEditProfile);

// Edit profile cancel button
$('#editProfileCancelButton').click(cancelEdit);

// Submit user profile changes button
$('#submitChangesButton').click(submitChanges);

// Move the cursor to the password field of login when enter is press in the email field
$("#emailInputLogin").keypress(function (e) {
    if (e.which == 13) {
        $("#passwordInputLogin").focus();
        $("#passwordInputLogin").select();
    }
});

// Sumbit login info on enter keypress in password field
$("#passwordInputLogin").keypress(function (e) {
    if (e.which == 13) {
        userLogin();
    }
});

// Functions Here

// Function to prompt the user for login or show them their profile if already logged int
function userLoginPrompt() {
    var status = $('#userLogin').val();
    console.log(status);
    if (status === 0) {
        console.log('Not currently logged in.');
        showLoginPrompt();
    }
    else {
        // start generating the profile view HTML elements.
        generateProfileView();
    }
}

/*
 * This function generates the HTML for the profile view. It will populate
 * the modal with the user's profile information.
 */
function generateProfileView() {
    // Create a string to be inserted into the profile view.
    var profile = '<strong>Name:</strong> ' + currUser + '</br>'
        + '<strong>Calendar ID:</strong> ' + calendarID + '</br>'
        + '<strong>Home Location:</strong> ' + home + '</br>'
        + '<strong>Voice Preference:</strong> ' + voicePref + '</br>';
    $('#userProfileData').empty();
    $('#userProfileData').append(profile);
    $('#userProfile').modal('show')
    console.log(currUser + ' is currently logged in.');
    generateForm();
}

// Function to show the login modal
function showLoginPrompt() {
    $('#loginPrompt').modal('show');
}

/*
 * Function attempts to log the user in using the authentication
 * endpoint. Additionally this button functions as a user action
 * to enable microphone detection and animation of the canvas
 * for the Remi animation. This addresses an issue with Google 
 * Chrome after version 66.
 */
function userLogin() {
    // Run the microphone check again and attempt to start the
    // animation for Remi.
    console.log('Attempt to log in a user...');
    // Get the values from the input fields
    var eField = $('#emailInputLogin').val();
    var pField = $('#passwordInputLogin').val();
    // Send the input to the Auth controller to be checked
    var source = "/auth/user/?email=" + eField + "&attemptedPassword=" + pField;
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: userLoginHelper,
        error: logFailure
    });

    

}

/*
 * This function is called when we receive a response from the
 * authentication endpoint. This handles the authentication response
 * from the endpoint. If it receive a valid authentication it will
 * get the user settings from the appropriate end point.
 */
function userLoginHelper(data) {
    //Check if login was valid or not
    console.log(data);
    authCheck = data.Authorized;
    if (authCheck) {
        console.log("Authorized User!");
        // Get the authenticated user's id
        uid = data.UID;
        console.log("UID = " + uid);
        // Make the user login value true. If 0 the user is not logged in.
        $('#userLogin').val(uid); 
        // Send the authenticated user's id to the user controller to get profile information
        source = "/user/prof/" + String(uid);
      console.log("source: " + source);
        $.ajax({
            type: "GET",
            dataType: "json",
            url: source,
            success: getDetails,
            error: logFailure
        });
    }
    else {
        console.log("Authorization Failed");
        // Get the Error message
        msg = data.ErrorMessage;
        console.log("Error: " + msg);

        //Display Error Message - Alert for now
        alert(msg);
    }
}
// Function to get the user's profile details
function getDetails(data) {
    console.log(data);
    // Get the user's profile data (saved to local memory as variables)
    currUser = data.Alias;
    home = data.HomeLocation;
    calendarID = encodeURIComponent(data.CalendarID);
    voicePref = data.VoicePref;
    micCheck();

    // Change the "Login" link to display the current user's alias
    $('#userLogin').html('<a class="navbar-brand" href="#">' + currUser + '</a > ');
    // Display the Logout link
    $('#userLogout').css('display', 'flex');

    // Hide login modal
    $('#loginPrompt').modal('hide');
    fillCheckboxes();
}

/*
 * This function will log the user out. It clears the canvas of any
 * information and clears any values that stored user information in
 * memory.
 */
function userLogout() {
    console.log('Log out the user...')
    // Stop displaying the logout link
    $('#userLogout').css('display', 'none'); 
     // Indicate in the value that user is logged out
    $('#userLogin').val(false);
     // Change the user info back to login
    $('#userLogin').html('<a class="navbar-brand" href="#">Login</a>');
    $('#logoutConfirmation').modal('hide');
    // Calls the clear remiOutput function to clear the output on the canvas.
    clearRemiOutput();
    // Resets all of the values stored in memory to null or empty values.
    currUser = "";
    home = "";
    calendarID = "";
    voicePref = "";
    uid = null;
    $('#checktextin').prop('checked', true);
    $('#checktextout').prop('checked', true);
    $('#checkVoiceOut').prop('checked', false);
}

// Function to show the logout modal
function showLogout() {
    $('#logoutConfirmation').modal('show');
}

// All the create account logic goes here ---------------

// Switch out modals
function createAccount() {
    $('#loginPrompt').modal('hide');
    $('#createPrompt').modal('show');
}

// Validate that the form is filled in 
function validateAccount() {
    console.log("Validating...");
    if (!checkFormFilled()) {
        registerFail();
        return;
    }
    // Send it over to the actual logic
    else sendCreate();
}

// A function to check form fields
function checkFormFilled() {
    if ($("#usernameInput").val() === "" || $("#emailInput").val() === "" || $("#passwordInput").val() === ""
        || $("#passwordConfirmInput").val() === "" || $("#aliasInput").val() === "" || $("#homeInput").val() === ""
        || $("#calendarInput").val() === "")
        return false;
    return true;
}

// Parse our variables and send the AJAX request
function sendCreate() {
    console.log("Attempting to create an account...");
    var username = $('#usernameInput').val();
    var email = $('#emailInput').val();
    var p1 = $('#passwordInput').val();
    var p2 = $('#passwordConfirmInput').val();
    var alias = $('#aliasInput').val();
    var home = $('#homeInput').val();
    var cal = $('#calendarInput').val();

    // generate us a very silly looking query string
    var source = '/create/user/?username=' + username + '&email=' + email + '&password=' + p1 + '&confPassword=' + p2 + '&alias=' + alias + '&homeLoc=' + home + '&calID=' + cal; 
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: confirmRegistration,
        error: registerFail
    });
}

// Confirmation to the user and reset the fields
function confirmRegistration(newUser) {
    if (newUser.UserID !== -1) {
        $('#createAccountForm').empty();
        $('#createPrompt').modal('hide');
        alert("Registration successful!");
    }
    else registerFail();
}

// Something went wrong
function registerFail() {
    alert("Something went wrong! Please make sure all fields are filled out correctly and try again");
}

function openEditProfile() {
    $('#userProfile').modal('hide');
    $('#editUserProfile').modal('show');
}

function cancelEdit() {
    $('#editUserProfile').modal('hide');
    $('#userProfile').modal('show');
}

// Submit edit user profile changes
function submitChanges() {
    //Variables to set new values
    var newAlias = $('#editNameInput').val();
    var newCalID = $('#editCalendarInput').val();
    var newHomeLoc = $('#editHomeLocationInput').val();
    var newVoicePref = $('#voicePref').val();
    var newTextIn = $('#editTextInPref').is(':checked');
    var newTextOut = $('#editTextOutPref').is(':checked');
    var newVoiceOut = $('#editVoiceOutPref').is(':checked');

    // If not empty (changes made) change the value of the var
    if (newAlias !== "") {
        console.log("New Alias: " + newAlias);
        alias = newAlias;
        currUser = newAlias;
        $('#userLogin').html('<a class="navbar-brand" href="#">' + alias + '</a > ');
    }
    else { newAlias = currUser; } // If no changes set New var to current value (Optimize later?)

    // If not empty (changes made) change the value of the var
    if (newCalID !== "") {
        console.log("New CalanderID: " + newCalID);
        calendarID = newCalID;
    }
    else { newCalID = calendarID; } // If no changes set New var to current value (Optimize later?)

    // If not empty (changes made) change the value of the var
    if (newHomeLoc !== "") {
        console.log("New Home Location: " + newHomeLoc);
        home = newHomeLoc;
    }
    // If no changes set New var to current value (Optimize later?)
    else { newHomeLoc = home; } 
    
    // If the voice pref is changed set it in voicePref global var
    if (newVoicePref !== "No Change") {
        console.log("New Voice Preference: " + newVoicePref);
        // Make immediate changes to profile values stored in global vars
        // Required to make immediate change in voice selection
        voicePref = newVoicePref;
    }
    // If no changes set New var to current value
    else { newVoicePref = voicePref; } 
    

    // Call the User controller to make appropriate changes to the profile DB entry
    var changes = '?alias=' + newAlias + '&homeLoc=' + newHomeLoc + '&calendarID=' + newCalID + '&voicePref=' + newVoicePref + '&textIn=' + newTextIn + '&textOut=' + newTextOut + '&voiceOut=' + newVoiceOut; 
    var source = '/user/edit/' + uid + changes

    // we are not expecting data back so data type and type are not required.
    $.ajax({
        url: source,
        success: submitChangesHelper,
        error: logFailure
    });
}

function submitChangesHelper() {
    $('#editUserProfile').modal('hide');
    fillCheckboxes();
    userLoginPrompt();
}

// Generate the form for users to edit their profile
function generateForm() {
    var form = '<form>'
        + '<div class="form-group">'
        + '<label for="nameEdit"><b>Name</b></label>'
        + '<input type="text" placeholder="' + currUser + '" name="nameEdit" class="form-control" id="editNameInput" />'
        + '</div>'
        + '<div class="form-group">'
        + '<label for="calendarEdit"><b>Calendar ID</b></label>'
        + '<input type="text" placeholder="' + calendarID + '" name="calendarEdit" class="form-control" id="editCalendarInput" />'
        + '</div>'
        + '<div class="form-group">'
        + '<label for="homeLocationEdit"><b>Home Location</b></label>'
        + '<input type="text" placeholder="' + home + '" name="homeLocationEdit" class="form-control" id="editHomeLocationInput" />'
        + '</div>'
        + '<div class="form-group">'
        + '<label for="voicePref"><b>Preferred Voice:</b></label>'
        + '<select class="form-control" id="voicePref">'
        + '<option>No Change</option>'
        + '<option>UK English Female</option>'
        + '<option>UK English Male</option>'
        + '<option>US English Female</option>'
        + '<option>Spanish Female</option>'
        + '<option>French Female</option>'
        + '<option>Deutsch Female</option>'
        + '<option>Italian Female</option>'
        + '<option>Greek Female</option>'
        + '<option>Hungarian Female</option>'
        + '<option>Turkish Female</option>'
        + '<option>Russian Female</option>'
        + '<option>Dutch Female</option>'
        + '<option>Swedish Female</option>'
        + '<option>Norwegian Female</option>'
        + '<option>Japanese Female</option>'
        + '<option>Korean Female</option>'
        + '<option>Chinese Female</option>'
        + '<option>Hindi Female</option>'
        + '<option>Serbian Male</option>'
        + '<option>Croatian Male</option>'
        + '<option>Bosnian Male</option>'
        + '<option>Romanian Male</option>'
        + '<option>Catalan Male</option>'
        + '<option>Australian Female</option>'
        + '<option>Finnish Female</option>'
        + '<option>Afrikans Male</option>'
        + '<option>Albanian Male</option>'
        + '<option>Arabic Male</option>'
        + '<option>Armenian Male</option>'
        + '<option>Czech Female</option>'
        + '<option>Danish Female</option>'
        + '<option>Esperanto Male</option>'
        + '<option>Hatian Creole Female</option>'
        + '<option>Icelandic Male</option>'
        + '<option>Indonesian Female</option>'
        + '<option>Latin Female</option>'
        + '<option>Latvian Male</option>'
        + '<option>Macedonian Male</option>'
        + '<option>Moldavian Male</option>'
        + '<option>Montenegrin Male</option>'
        + '<option>Polish Female</option>'
        + '<option>Portuguese Female</option>'
        + '<option>Slovak Female</option>'
        + '<option>Spanish Latin American Female</option>'
        + '<option>Swahili Male</option>'
        + '<option>Tamil Male</option>'
        + '<option>Thai Female</option>'
        + '<option>Vietnamese Male</option>'
        + '<option>Welsh Male</option>'
        + '</select>'
        + '</div>'
        + '<div class="form-group form-check">'
        + '<input class="form-check-input" type="checkbox" name="editTextInPref" id="editTextInPref" />'
        + '<label for="editTextInPref" class="form-check-label"><b>Enable Text-input</b></label>'
        + '</div>'
        + '<div class="form-group form-check">'
        + '<input class="form-check-input" type="checkbox" name="editTextOutPref" id="editTextOutPref" />'
        + '<label for="editTextOutPref" class="form-check-label"><b>Enable Text-output</b></label>'
        + '</div>'
        + '<div class="form-group form-check">'
        + '<input class="form-check-input" type="checkbox" name="editVoiceOutPref" id="editVoiceOutPref" />'
        + '<label for="editVoiceOutPref" class="form-check-label"><b>Disable Voice Output</b></label>'
        + '</div>'
        + '</form>';
    $('#editData').empty();
    $('#editData').append(form);
    fillCheckboxes();
}

function fillCheckboxes() {

    var source = '/user/checkboxes/' + uid;

    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: function (data) {
            // User pref edits
            $('#editTextInPref').prop('checked', data["textIn"]);
            $('#editTextOutPref').prop('checked', data["textOut"]);
            $('#editVoiceOutPref').prop('checked', data["voiceOut"]);

            // Main checkboxes edits
            $('#checktextin').prop('checked', data["textIn"]);
            $('#checktextout').prop('checked', data["textOut"]);
            $('#checkVoiceOut').prop('checked', data["voiceOut"]);

            toggleRemisTextOutput();
            toggleTextInput();
            console.log("End of ajax success function");
        },
        error: function () {
            console.log("Something went wrong");
        }
    });
}