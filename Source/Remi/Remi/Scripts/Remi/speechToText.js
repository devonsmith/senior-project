﻿// JS to handle taking in and processing voice with the Web Speech API - Not using Bing Speech; after 3 days of trying to get it to work I could not.
// Inspired by: https://developer.mozilla.org/en-US/docs/Web/API/Web_Speech_API/Using_the_Web_Speech_API#Speech_recognition

// Initiallize the Speech Recongition API
// Piped vars are for Google Chrome Support
var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition
var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList
var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent
// The grammar for our Speech to Text call
var grammar = '#JSGF V1.0;' 
// Setup for the Speech Recognition
var recognition = new SpeechRecognition();
var speechRecognitionList = new SpeechGrammarList();

// Get our grammar
speechRecognitionList.addFromString(grammar, 1);
recognition.grammars = speechRecognitionList;

// Set the Recognition Language
recognition.lang = 'en-US';
// Set to allow/deny partial results: Deny (false)
recognition.interimResults = false;
// Set to allow 1 or more result alternatives: 1 - Get the best one only
recognition.maxAlternatives = 1;

// Listener for the button click (id=record)
$("#record").click(startRec);

// Start the Recording of the Command
function startRec() {
    // Start the recognizer
    // try/catch block so if recording has started, it doesn't break
    try {
        if (isLoggedIn()) {
            recognition.start();
            console.log('Logged in and ready to receive a command.'); //Print to console for testing
        }
        else {
            outputRemiChat('Oh no! You\'re not logged in. Please log in and try again.')
            console.log('Not logged in. Mic is not recording.'); //Print to console for testing
        }
    }
    catch (e) { }    // do nothing
}

// Get the API result
recognition.onresult = function (event) {
    // The last entry in the results
    var last = event.results.length - 1;
    // The command from the recorded speech
    var cmd = event.results[last][0].transcript;

    console.log('Confidence: ' + event.results[0][0].confidence); //Print to console for testing
    console.log(cmd); //Print to console for testing
    var inputText = replaceSpaces(cmd);
    console.log(inputText);
    // Place the detected text in the input box.
    $('#remitextinput').val(cmd);
    processTextLanguage(inputText);
}

// Stop speech regonition when there is a long pause
recognition.onspeechend = function () {
    // Stop the recognizer
    recognition.stop();
}