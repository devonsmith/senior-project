﻿// --- Global Variables here ---
//...

// --- Listeners here ---
//Temp button to demo the response for when Remi understands the user's intent
//$("#remisValidLanguageRequest").click(validInput);
//Temp button to demo the response for when Remi does NOT understand the user's intent
//$("#remisInvalidLanguageRequest").click(invalidInput);
//Button to submit text to Remi
$("#submitremitextinput").click(sendRemiTextInput);
// Enter key logic for text input to Remi
$("#remitextinputform").keypress(function (e) {
    if (e.which == 13) {
        sendRemiTextInput();
    }
});
//--- Functions here ---

// Function to let Remi know if the user is logged in or not
// Some refactoring may be required once real auth is put in
function isLoggedIn() {
    var userStatus = $("#userLogin").val();
    //console.log("userStatus: " + userStatus)
    if (userStatus == 0) {
        return false;
    }
    else {
        return true;
    }
}

// Function to send Remi's input to LUIS via processTextLanguage()
function sendRemiTextInput() {
    if (isLoggedIn()) {
        var inputText = document.getElementById("remitextinput").value;
        // console.log(inputText);
        // Sending input the JSON, when you have spaces, replace with %20
        inputText = replaceSpaces(inputText);
        console.log(inputText);
        processTextLanguage(inputText);
    }
    else {
        outputString = "Oh no! You\'re not logged in. Please log in and try again.";
        outputRemiChat(outputString);
    }
}

// Replaces spaces with %20
function replaceSpaces(inputText) {
    var replacedText = inputText.split(" ").join("%20");
    return replacedText;
}

// Function to send AJAX request off to our server
// The users text input is added as a query string to the source
function processTextLanguage(input) {
    if (input.length <= 0) {
        input = "Hello"
    }
    var source = "/language/text/?q=" + input;
    console.log(source);
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: doIntent,
        error: logFailure
    });
}

// Function to indicate that the AJAX call broke. (For debugging)
function logFailure(response) {
    console.log(response);
    var outputString = 'Oh no, looks like I am having issues with my AJAX logic.';
    outputRemiChat(outputString);
}

// Function to determine User intent
function getIntent(response) {
    // Empty the filler pane for reuse.
    $('#fillerPane').empty();
    console.log(response);
    var intent = response.topScoringIntent.intent;

    return intent;
}

function getEntities(response) {
    var entities = response.entities;

    return entities;
}

//Do Stuff with the intent
function doIntent(response) {
    var intent = getIntent(response);
    var entities = getEntities(response);

    switch (intent) {
        case ('RemiGreeting'):
            outputString = 'Hello ' + currUser + ', how can I help you?';
            isTime = false;
            outputRemiChat(outputString);
            break;

        case ('RemiJoke'):
            isTime = false;
            getJoke();
            break;

        case ("GetCalendarData"):
            isTime = false;
            getPublicEvents(entities);
            break;

        case ("GetWeather"):
            isTime = false;
            getWeather(entities);
            break;

        case ("GetDefinition"):
            isTime = false;
            getDefinition(entities);
            break;

        case ("User.Logout"):
            isTime = false;
            showLogout();
            break;

        case ("Search"):
            isTime = false;
            entitySearch(entities);
            break;

        case ("GetCurrentTime"):
            isTime = true;
            getCurrentTime();
            break;
        case ("Intro"):
            introduction();
            break;
        case ("WrapUp"):
            conclusion();
            break;

        default:
            outputString = "I am sorry, I do not understand your request. Please rephrase it and try again.";
            isTime = false;
            outputRemiChat(outputString);
            break;
    }
}
// Helper function to update the message output to the canvas.
function outputRemiChat(remisResponse) {
    remiSpeech = remisResponse;
    // Check if mute is toggled
    var muted = $("input[name=muteVoiceOutput]:checked").length;
    // Call speech function if input is valid & the mute is not toggled
    if (remiSpeech.toString().length != 0 && muted == 0) voiceOutput(remiSpeech);
    // console.log("Response: " + remiSpeech);
}

function clearRemiOutput() {
    remiSpeech = '';
}

// --- Functions to enable Remi's sense of Humor ---
// --- These functions may move to another JS file in a refactor once we have more Functionality for Remi ---
// Function to get a joke from an API
function getJoke() {
    var source = "/language/joke";
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        success: sendJoke,
        error: logFailure
    });
}

// Function to send the joke to Remi to display to the user
function sendJoke(response) {
    var outputString = response.joke;
    outputRemiChat(outputString);
}

// Takes in a string, sets it all to lower case and then capitalized the first letter of every word by splitting at the space
// param: String
function initialCaps(incomingString) {
    var returnString = "";
    var tempArray = null;

    incomingString = incomingString.toLowerCase();

    tempArray = incomingString.split(" ");

    for (var i = 0; i < tempArray.length; i++) {
        returnString += tempArray[i].charAt(0).toUpperCase() + tempArray[i].substring(1, tempArray[i].length) + " ";
    }

    returnString = returnString.trim();

    return returnString;
}


// Function to use ResponsiveVoice JS API to give Remi a voice
// PARAM: string for Remi to speak
function voiceOutput(outputText) {
    // TODO clear out testing stuff
    // Just to see if it's working...
    console.log("Made it to the function...");
    console.log("Input: " + outputText);
    console.log("Voice Pref: " + voicePref);

    responsiveVoice.speak(outputText, voicePref);
}

// Takes speech input from the user and performs a search with it.
function entitySearch(entities) {
    var search = '/EntitySearch/EntitySearch/?query=';
    for (var i = 0; i < entities.length; ++i) {
        if (entities[i].type === 'Subject') {
            console.log("Subject: " + entities[i].entity.toLowerCase());
            search += entities[i].entity.toLowerCase();
        }
        else if (entities[i].type === 'Zip') {
            search += " " + entities[i].entity.toLowerCase();
        }
    }
    console.log("Search Query: " + search);
    $.ajax({
        type: "GET",
        dataType: "json",
        url: search,
        success: function (data) { outputSearch(data); },
        error: searchFailure
    });
}

function outputSearch(data){
    console.log('Information back from search: ');
    console.log(data);
    // is it places or entities?
    if (data.entities != null) {
        $('#entityTestOutput').empty();
        $('#entityTestOutput').append(data.entities.value[0].name);
        outputString = data.entities.value[0].description;
    }
    else if (data.places != null) {
        // Since this was a list of places we'll output the information
        // in an output pane.
        // Empty the filler pane to prepare for new info
        $('#canvasOverlay').empty();
        var entityPrintOut = '';
        var places = data.places.value;
        entityPrintOut = '<dl>';
        for (let place of places) {
            entityPrintOut += '<dt>' + place.name + '</dt>'
                + '<dd>' + '<a href="' + place.url + '">' + place.url + '</a></dd>';
        }
        entityPrintOut += '</dl>'
        $('#canvasOverlay').append(entityPrintOut);

        setTimeout(function () {
            $('#canvasData').modal('show');
        }, 0.5 * 1000);

        outputString = 'Here are some of the results from your query.';
    }
    else {
        // Search of some unknown type?
        outputString = 'I\'m so sorry! I wasn\'t able to find that information for you.';
    }
    outputRemiChat(outputString);
}

function searchFailure(){
    outputString = "I'm so sorry! I wasn't able to find that information for you.";
    outputRemiChat(outputString);
}

// Function to get the current time. This will get the time local time via javascript
function getCurrentTime() {
    var currentTime = new Date(); // Creates a date in milliseconds from the epoch

    var timeString = getTimeString();

    remiSpeech = currentTime.toLocaleTimeString("en-us", { hour: '2-digit', minute: '2-digit' });
    var muted = $("input[name=muteVoiceOutput]:checked").length;
    if (muted == 0) voiceOutput(timeString);
}

// Split off making the timeString into a seperate function for testing
function getTimeString() {
    var currentTime = new Date(); // Creates a date in milliseconds from the epoch

    var timeString = "It is currently " + currentTime.toLocaleTimeString("en-us", { hour: '2-digit', minute: '2-digit' }) + ".";

    // Populate the invisible div for selenium testing
    $('#entityTestOutput').empty();
    $('#entityTestOutput').append(timeString);

    return timeString;
}

// WOU AES 2018 presentation Introduction
function introduction() {
    var intro =
        "Hello Everyone, my name is Remi. I am a conversational AI for your web browser. " +
        "I can understand you by listening for your intents during our conversations, but there is still a lot for me to learn. " +
        "I would like to help you make your daily life less stressful by managing your calendar, telling you the weather, telling jokes, telling you the time, and searching the web. " +
        "Now, please welcome Abby, Devon, Jake, and Stephen to the podium; they will tell you more about me.";

    outputRemiChat(intro);
}


// WOU AES 2018 presentation conclusion/wrap-up
function conclusion() {
    var concl =
        "Thank you, everyone, for your attention. I was so happy to be able to introduce myself to you. " +
        "I would also like to thank all of the professors here a Western Oregon University for passing on your knowledge to my creators. " +
        "Also, thank you to all of the parents and friends in the audience; " +
        "I owe my very existance to your constant support. " +
        "Everyone, have a wonderful rest of your day and I hope that we can become great friends in the future. " +
        "How about one last joke before I sign off? " +
        "Human: What do we want? Computer: Natural language processing! Human: When do we want it? Computer: When do we want what? " +
        "Ha ha, funny stuff. Anyway, goodbye everyone!";

    outputRemiChat(concl);
}