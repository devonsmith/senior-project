﻿using System;
using System.Collections.Generic;

namespace Remi.Models.DAL
{
    public interface IRepository : IDisposable
    {
        // User data
        IEnumerable<User> GetUsers();
        User GetUserByEmail(string email);
        User GetUserByUserID(int userId);
        void InsertUser(User user);
        void DeleteUser(string email);
        void UpdateUser(User user);

        // Profile data
        IEnumerable<Profile> GetProfiles();
        Profile GetProfileByID(int profileId);
        Profile GetProfileByUserID(int userId);
        void InsertProfile(Profile profile);
        void DeleteProfile(int profileId);
        void UpdateProfile(Profile profile);

        void Save();
    }
}