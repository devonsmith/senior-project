﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Remi.Models.DAL
{
    public class Repository : IRepository, IDisposable
    {
        private UserContext context;

        public Repository(UserContext context)
        {
            this.context = context;
        }

        public IEnumerable<User> GetUsers()
        {
            return context.Users.ToList();
        }

        public User GetUserByEmail(string email)
        {
            return context.Users.Where(u => u.Email == email).FirstOrDefault();
        }

        public User GetUserByUserID(int userId)
        {
            return context.Users.Find(userId);
        }

        public void InsertUser(User user)
        {
            context.Users.Add(user);
        }

        public void DeleteUser(string email)
        {
            User user = context.Users.Find(email);
            context.Users.Remove(user);
        }

        public void UpdateUser(User user)
        {
            context.Entry(user).State = EntityState.Modified;
        }

        IEnumerable<Profile> IRepository.GetProfiles()
        {
            return context.Profiles.ToList();
        }

        public Profile GetProfileByID(int profileId)
        {
            return context.Profiles.Find(profileId);
        }

        public Profile GetProfileByUserID(int userId)
        {
            return context.Profiles.Where(p => p.UserID == userId).FirstOrDefault();
        }

        public void InsertProfile(Profile profile)
        {
            context.Profiles.Add(profile);
            //throw new NotImplementedException();
        }

        public void DeleteProfile(int profileId)
        {
            Profile profile = context.Profiles.Find(profileId);
            context.Profiles.Remove(profile);
        }

        public void UpdateProfile(Profile profile)
        {
            context.Entry(profile).State = EntityState.Modified;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}