﻿using Remi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using Remi.Models.DAL;
//using System.Diagnostics;
using System.Text.RegularExpressions;
using Remi.Models.DAL;

namespace Remi.Controllers
{

    public class AuthenticationController : Controller
    {
        //private UserContext db = new UserContext();
        private static string salt = "2F21ZZ1arM";
        private IRepository userRepository;

        public AuthenticationController()
        {
            this.userRepository = new Repository(new UserContext());
        }

        public AuthenticationController(IRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        //private UserContext db = new UserContext();

        public JsonResult IsUserAuthenticated(string email, string attemptedPassword)
        {
            //var userExists = db.Users.Where(u => u.Email == email).FirstOrDefault();
            var userExists = userRepository.GetUserByEmail(email);

            Dictionary<string, Object> authUser = new Dictionary<string, Object>();
            string cipheredPassword = "";
            string saltString = "";
            string decipheredPassword = "";

            if (userExists == null) // if email doesn't exist, we can't authorize
            {
                // Add the Auth User
                authUser = AddAuthUserInvalidLogin();
            }
            else
            {
                //cipheredPassword = db.Users.Where(u => u.Email == email).FirstOrDefault().CipheredPassword;
                cipheredPassword = userRepository.GetUserByEmail(email).CipheredPassword;
                //saltString = db.Users.Where(u => u.Email == email).FirstOrDefault().PasswordSalt;
                saltString = userRepository.GetUserByEmail(email).PasswordSalt;

                decipheredPassword = Decrypt(cipheredPassword, saltString);

                if (attemptedPassword == decipheredPassword) // the attemptedPassword is correct
                {
                    // Add the Auth User
                    var uid = userRepository.GetUserByEmail(email).UserID;
                    Console.Write("auth userid: " + uid);
                    //Debug.WriteLine("uid=" + uid);
                    authUser = AddAuthUserValidated(uid);
                }
                else // invalid password attempt
                {
                    // Add the Auth User
                    authUser = AddAuthUserInvalidLogin();
                }
            }
            return Json(authUser, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public static Dictionary<String, object> AddAuthUserInvalidLogin()
        {
            Dictionary<String, object> authUser = new Dictionary<String, object>();
            authUser.Add("Authorized", false);
            authUser.Add("ErrorMessage", "Invalid Email and/or Password");
            authUser.Add("UID", null);
            return authUser;
        }

        [NonAction]
        public static Dictionary<String, object> AddAuthUserValidated(int uid)
        {
            Dictionary<String, object> authUser = new Dictionary<String, object>();
            authUser.Add("Authorized", true);
            authUser.Add("ErrorMessage", null);
            authUser.Add("UID", uid);
            return authUser;
        }


        // Cipher and Decipher From https://stackoverflow.com/questions/10168240/encrypting-decrypting-a-string-in-c-sharp
        // allows us to cipher and decipher our passwords

        // This constant is used to determine the keysize of the encryption algorithm in bits.
        // We divide this by 8 within the code below to get the equivalent number of bytes.
        private const int Keysize = 256;

        // This constant determines the number of iterations for the password bytes generation function.
        private const int DerivationIterations = 1000;

        /// <summary>
        /// encrypt password using a salt string
        /// </summary>
        /// <param name="passwordToUse">String the password in plain text</param>
        /// <param name="saltString">String the salt used to encode the password</param>
        /// <returns>String of 128 chars in Base64</returns>
        [NonAction]
        public static string Encrypt(string passwordToUse, string saltString)
        {
            // Salt and IV is randomly generated each time, but is preprended to encrypted cipher text
            // so that the same Salt and IV values can be used when decrypting.  
            var saltStringBytes = Generate256BitsOfRandomEntropy();
            var ivStringBytes = Generate256BitsOfRandomEntropy();
            var plainTextBytes = Encoding.UTF8.GetBytes(passwordToUse);
            using (var password = new Rfc2898DeriveBytes(saltString, saltStringBytes, DerivationIterations))
            {
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                // Create the final bytes as a concatenation of the random salt bytes, the random iv bytes and the cipher bytes.
                                var cipherTextBytes = saltStringBytes;
                                cipherTextBytes = cipherTextBytes.Concat(ivStringBytes).ToArray();
                                cipherTextBytes = cipherTextBytes.Concat(memoryStream.ToArray()).ToArray();
                                memoryStream.Close();
                                cryptoStream.Close();
                                return Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Password decoding function
        /// </summary>
        /// <param name="cipherText">String of the Base64 encoded password.</param>
        /// <param name="saltString">String the string used to encode and decode the password</param>
        /// <returns></returns>
        [NonAction]
        public static string Decrypt(string cipherText, string saltString)
        {
            // Get the complete stream of bytes that represent:
            // [32 bytes of Salt] + [32 bytes of IV] + [n bytes of CipherText]
            var cipherTextBytesWithSaltAndIv = Convert.FromBase64String(cipherText);
            // Get the saltbytes by extracting the first 32 bytes from the supplied cipherText bytes.
            var saltStringBytes = cipherTextBytesWithSaltAndIv.Take(Keysize / 8).ToArray();
            // Get the IV bytes by extracting the next 32 bytes from the supplied cipherText bytes.
            var ivStringBytes = cipherTextBytesWithSaltAndIv.Skip(Keysize / 8).Take(Keysize / 8).ToArray();
            // Get the actual cipher text bytes by removing the first 64 bytes from the cipherText string.
            var cipherTextBytes = cipherTextBytesWithSaltAndIv.Skip((Keysize / 8) * 2).Take(cipherTextBytesWithSaltAndIv.Length - ((Keysize / 8) * 2)).ToArray();

            using (var password = new Rfc2898DeriveBytes(saltString, saltStringBytes, DerivationIterations))
            {
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new MemoryStream(cipherTextBytes))
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                            {
                                var plainTextBytes = new byte[cipherTextBytes.Length];
                                var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                memoryStream.Close();
                                cryptoStream.Close();
                                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// provides randomness to the encrypting function
        /// </summary>
        /// <returns>An array of randomly generated bytes.</returns>

        private static byte[] Generate256BitsOfRandomEntropy()
        {
            var randomBytes = new byte[32]; // 32 Bytes will give us 256 bits.
            using (var rngCsp = new RNGCryptoServiceProvider())
            {
                // Fill the array with cryptographically secure random bytes.
                rngCsp.GetBytes(randomBytes);
            }
            return randomBytes;
        }


        /// <summary>
        /// Validate the new user's information before we officially add them to the database
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="email">Email Address</param>
        /// <param name="password">Password</param>
        /// <param name="confPassword">Password Confirmation</param>
        /// <param name="alias">Preferred Name</param>
        /// <param name="homeLoc">Home Location in City, State format</param>
        /// <param name="calID">Public Google calendar ID</param>
        public JsonResult ValidateNewUser(string username, string email, string password, string confPassword, string alias, string homeLoc, string calID)
        {
            // start with a true bool to check at the end
            var isValid = true;
            User newUser = new User
            {
                UserID = -1
            };

            // Make sure we got actual values in our query string; if not, throw an alert and have the user try again
            // Note: we don't use the isValid variable here because if these strings are empty or null we will have a problem with the rest of the logic in this function
            if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(email) || string.IsNullOrEmpty(password) || String.IsNullOrEmpty(confPassword) || String.IsNullOrEmpty(alias) || String.IsNullOrEmpty(homeLoc) || String.IsNullOrEmpty(calID))
            {
                CreateFail();
                return Json(newUser, JsonRequestBehavior.AllowGet);
            }

            // Check if the user exists already (no duplicate emails!)
            if (DoesUserExist(email)) isValid = false;

            // Check if passwords match
            if (!DoPasswordsMatch(password, confPassword)) isValid = false;

            // Check if password is long enough
            if (!IsPasswordLongEnough(password)) isValid = false;

            // Check Email
            if (!IsValidEmail(email)) isValid = false;

            //Calendar ID doesn't follow, necesarily, standard e-mail form, it simply must be a non-empty string which is already checked.

            // Parse the city and state into an array
            // 0 = city, 1 = state
            string[] homeLocArr = GetCityState(homeLoc);
            if (homeLocArr == null) isValid = false; // Check that the parsing worked

            // Okay, seems alright. Let's add this.
            if (isValid)
            {
                // Add the new user and get the user ID for the new user
                var userID = AddNewUser(new Models.User(), username, email, password);
                if (userID != -1)
                {
                    // Use that user ID to add their profile as well
                    AddNewUserProfile(new Models.Profile(), alias, homeLoc, homeLocArr[0], homeLocArr[1], calID, userID);
                    newUser.UserID = userID;
                }

                return Json(newUser, JsonRequestBehavior.AllowGet);
            }

            // Nope, throw an alert
            else CreateFail();
            return Json(newUser, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds the validated new user to the database after encrypting their password
        /// </summary>
        /// <param name="user">User object</param>
        /// <param name="username">Username</param>
        /// <param name="email">Email</param>
        /// <param name="password">Unencrypted password</param>
        /// <returns>Returns UserID of the new user object if successful, otherwise returns -1</returns>
        public int AddNewUser([Bind(Include = "UserName,Email,CipheredPassword,PasswordSalt")] User user, string username, string email, string password)
        {
            // Encrypt password
            password = Encrypt(password, salt);

            // Add info to Users and Profiles table
            user.Email = email;
            user.UserName = username;
            user.CipheredPassword = password;
            user.PasswordSalt = salt;

            if (ModelState.IsValid)
            {
                //db.Users.Add(user);
                userRepository.InsertUser(user);
                userRepository.Save();
                //db.SaveChanges();
                return user.UserID; // return the user ID to show that it worked
            }
            // return -1 to show that it didn't work 
            else { return -1; }
            }

        /// <summary>
        /// Adds a user profile for the newly created user
        /// </summary>
        /// <param name="profile">Profile object</param>
        /// <param name="alias">Preferred name</param>
        /// <param name="homeLoc">Home location in City, State format</param>
        /// <param name="homeCity">String of home city</param>
        /// <param name="homeState">String of home state</param>
        /// <param name="calID">Public Google calendar ID</param>
        /// <param name="userID">User ID to which to bind the profile</param>
        public void AddNewUserProfile([Bind(Include = "Alias, HomeLoc, HomeCity, HomeState, CalendarID,  UserID")] Profile profile, string alias, string homeLoc, string homeCity, string homeState, string calID, int userID)
        {
            
            profile.Alias = alias;
            profile.HomeLoc = homeLoc;
            profile.HomeCity = homeCity;
            profile.HomeState = homeState;
            profile.CalendarID = calID;
            profile.UserID = userID;
            profile.VoicePref = "UK English Female";

            if (ModelState.IsValid)
            {
                userRepository.InsertProfile(profile);
                userRepository.Save();
                return;
            }
            else { CreateFail(); }
        }

        /// <summary>
        /// Called if the regsitration failed somewhere in the C# logic
        /// </summary>
        /// <returns>Returns to the Javscript to alert the user to the failure.</returns>
        public JavaScriptResult CreateFail() 
        {
            return JavaScript("registerFail");
        }

        /// <summary>
        /// Checks email against database to make sure it isn't a duplicate
        /// </summary>
        /// <param name="email">Email to check</param>
        /// <returns>True if the user already exists</returns>
        public bool DoesUserExist(string email)
        {
            //var userExists = db.Users.Where(u => u.Email == email).FirstOrDefault();
            var userExists = userRepository.GetUserByEmail(email);
            if (userExists != null)
            {
                return true;
            }
            else { return false; }
        }

        /// <summary>
        /// Checks to make sure the user entered the same password both times
        /// </summary>
        /// <param name="p1">Password one</param>
        /// <param name="p2">Confirmation password</param>
        /// <returns>True if passwords match</returns>
        public bool DoPasswordsMatch(string p1, string p2)
        {
            if (String.Equals(p1, p2)) return true;
            return false;
        }

        /// <summary>
        /// Check and see if the password is at minimum 4 characters
        /// </summary>
        /// <param name="p">password to check</param>
        /// <returns></returns>
        public bool IsPasswordLongEnough(string p)
        {
            if (p.Length >= 4) return true;
            return false;
        }

        /// <summary>
        /// Uses Regex to check if email entered is in correct format
        /// </summary>
        /// <param name="email">Email to check</param>
        /// <returns>True if a valid format</returns>
        public bool IsValidEmail(string email)
        {
            Regex regex = new Regex("^[A-Z0-9._%+-]+@[A-Z0-9]+.[A-Z]{2,4}$", RegexOptions.IgnoreCase);
            return regex.IsMatch(email);
        }

        /// <summary>
        /// Parse the homeLoc string into a string array where city = 0 and state = 1
        /// </summary>
        /// <param name="input">Input string of home location</param>
        /// <returns></returns>
        public string[] GetCityState(string input)
        {
            Regex regex = new Regex("^[A-Za-z\x20]+,[A-Za-z\x20]+$");
            if (regex.IsMatch(input))
            {
                
                string[] result = Regex.Split(input, ",");
                return result;
            }
            return null;
        }
    }
}