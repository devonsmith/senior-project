﻿using Remi.Models;
using Remi.Models.DAL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Remi.Controllers
{
    public class UserController : Controller
    {
        private IRepository userRepository;

        public UserController()
        {
            this.userRepository = new Repository(new UserContext());
        }

        public UserController(IRepository userRepository)
        {
            this.userRepository = userRepository;
        }


        // User context for user database access.
        // private UserContext db = new UserContext();
        /// <summary>
        /// Returns the user's preferences to them for inspection.
        /// </summary>
        /// <param name="id">The identifier of the user. </param>
        /// <returns>A Json response containing the user data.</returns>
        public JsonResult UserPreferences(int id)
        {
            // Get the user from the database that matches the requested ID.
            //var user = db.Profiles.Where(u => u.ProfileID == id).FirstOrDefault();
            var profile = userRepository.GetProfileByUserID(id);

            // Generate a dictionary to pass as a Json object.
            Dictionary<string, string> userProperties = new Dictionary<string, string>
            {
                {"Alias", profile.Alias},
                {"VoicePref", profile.VoicePref },
                {"HomeLocation", profile.HomeLoc},
                {"CalendarID", profile.CalendarID}
            };

            // Return the user properties as a Json request.
            return Json(userProperties, JsonRequestBehavior.AllowGet);
        }

        // User context for user database access.
        // private UserContext db = new UserContext();
        /// <summary>
        /// Updates the database with the new profile data specified by the user
        /// in the edit profile modal/form
        /// </summary>
        /// <param name="id">The identifier of the user. </param>
        /// <param name="alias">The new Alias;</param>
        /// <param name="calendarID">The new Calendar ID;</param>
        /// <param name="homeLoc">The new Home Location;</param>
        /// <param name="voicePref">The new Voice Preference;</param>
        /// <param name="textIn">Checkbox value for text input</param>
        /// <param name="textOut">Checkbox value for text output</param>
        /// <param name="voiceOut">Checkbox value for voice output</param>
        public void sumbitUserProfileChanges(int id, string alias, string homeLoc, string calendarID, string voicePref, bool textIn, bool textOut, bool voiceOut)
        {
            var profile = userRepository.GetProfileByUserID(id);

            // Debug stuff to make sure we are getting the correct data
            Debug.WriteLine("id: " + id);
            Debug.WriteLine("alias: " + alias);
            Debug.WriteLine("homeLoc: " + homeLoc);
            Debug.WriteLine("calendarID: " + calendarID);
            Debug.WriteLine("voicePref: " + voicePref);
            Debug.WriteLine("text input: " + textIn);
            Debug.WriteLine("text output: " + textOut);
            Debug.WriteLine("voice output: " + voiceOut);

            // Rough Update to fix Profile Edit bug:
            // Simply take in everything changed or not

            profile.Alias = alias;
            profile.HomeLoc = homeLoc;
            profile.HomeCity = getCityState(homeLoc)[0];
            profile.HomeState = getCityState(homeLoc)[1];
            profile.CalendarID = calendarID;
            profile.VoicePref = voicePref;
            profile.InputText = textIn;
            profile.OutputText = textOut;
            profile.VoiceOutput = voiceOut;

            // And then save the changes and update profile table.
            userRepository.Save();
            userRepository.UpdateProfile(profile);
        }

        public JsonResult GetCheckboxPrefs(int id)
        {
            var profile = userRepository.GetProfileByUserID(id);

            Dictionary<String, bool> checkboxes = new Dictionary<string, bool>
            {
                {"textIn", profile.InputText },
                {"textOut", profile.OutputText},
                {"voiceOut", profile.VoiceOutput }
            };
            return Json(checkboxes, JsonRequestBehavior.AllowGet);
        }

        public string[] getCityState(string input)
        {
            Regex regex = new Regex("^[A-Za-z\x20]+,[A-Za-z\x20]+$");
            if (regex.IsMatch(input))
            {

                string[] result = Regex.Split(input, ",");
                return result;
            }
            return null;
        }
    }
}