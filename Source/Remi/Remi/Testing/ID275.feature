Feature: As a user, I want to reliably get to the functions I want using natural language, so that I don't get information I'm not interested in.

Scenario: The user is requesting the time
	Given the user is logged in 
	And voice output is working
	When the user asks "What time is it?"
	Then Remi responds by saying "It is currently [time]".
	
Scenario: The user is requesting the time
	Given the user is logged in 
	And voice output is working
	When the user asks "What is the time?"
	Then Remi responds by saying "It is currently [time]".
	
Scenario: The user is requesting the time
	Given the user is logged in 
	And voice output is working
	When the user asks "What is the hour?"
	Then Remi responds by saying "It is currently [time]".