Feature: I want to chose a different voice for Remi so that I can personalize my experience

Scenario: I have not set a default voice for Remi in my profile
	Given I am logged in 
	And voice output is working
	When I say "Hello" to Remi
	Then Remi responds by saying "Hello, [my_name]" in the default voice, UK English Female
	
Scenario: I have chosen a new default voice for Remi in my profile
	Given I am logged in
	And I have selected "US English Female" as Remi's default voice in my profile
	When I say "Hello" to Remi
	Then Remi responds in the correct voice, US English Female 
	
Scenario: Remi's default voice is always UK English Female 
	Given I am not logged in 
	When I say "Hello" to Remi 
	Then Remi responds by prompting me to login, in the default voice profile UK English Female 
	
Scenario: I consider changing my default voice but change my mind.
	Given I am logged in
	And I go to edit my profile
	When I change my preferred voice to "US English Female" and then cancel out of the screen without saving
	When I say "Hello" to Remi 
	Then Remi responds by greeting me in the default voice, UK English Female