Feature: User can update profile information

Background:
  Given a user named "Test"
  And a user named "SomeoneElse"
  And a profile for "Test"
  And a profile for "SomeoneElse"
  And "Test" is logged in
  And "Test" has clicked their name in the NavBar

Scenario: There is an "Edit Profile" button on the Profile Modal
when the Profile modal opens
Then there should be a button named "Edit Profile" in that modal

Scenario: "Edit Profile" form is in a Modal
When the user clicks the "Edit Profile" button
Then a modal pops up contained the form for editing that user's profile

Scenario: The "Edit Profile" modal has a "Cancel" button
When the user clicks the "Edit Profile" Button
Then a button named "Cancel" should be present on the resulting modal

Scenario: The user clicks the "Cancel" button
Given that the user is viewing the "Edit Profile" modal
When the user clicks the "Cancel" button
Then the "Edit Profile" modal should become hidden
And the "Profile" modal for that user should be shown

Scenario: The user can edit their alias
When the user clicks the "Edit Profile" button
And the user enters "Test User" in the alias field
Then user clicks the "Submit" button if finished

Scenario: The user can edit their Calendar ID
When the user clicks the "Edit Profile" button
And the user enters "1234rdg@example.com" in the Calendar ID field
Then user clicks the "Submit" button if finished

Scenario: The user can edit their Home Location
When the user clicks the "Edit Profile" button
And the user enters "Portland, Maine" in the Home Location field
Then user clicks the "Submit" button if finished

Scenario: The "Edit Profile" modal has a "Submit" button
When the user clicks the "Edit Profile" button
Then a button named "Submit" should be present on the resulting modal

Scenario: The user clicks the "Submit" button
Given that the user is viewing the "Edit Profile" modal
When the user clicks the "Submit" button
Then all data inputted into the form fields is immediately updated in memory
And all data inputted into the form fields is immediately updated in the DB
And all data with related fields that are empty shouldn't be changed
And the "Edit Profile" modal should be emptied
And the "Edit Profile" modal should become hidden
And the "Profile" modal should become visible
And the new data should appear on the "Profile" modal

Scenario: The user cannot edit other users' profiles
When I click the "Edit Profile" button
Then I shouldn't be able to see or edit the profile of "SomeoneElse"
